FROM teracy/ubuntu:20.04-dind-latest

# Update the package repository and install Docker
RUN apt-get update && \
    apt-get install -y \
    sudo \
    wget \
    dnsutils \
    openssl 

# Créez les répertoires pour stocker les certificats, utilisateurs et le compose
RUN mkdir -p /certificates
RUN mkdir -p /ldap_user
RUN mkdir -p /etc/docker/compose
RUN openssl dhparam -out /certificates/dhkey.pem 2048

# Copiez le script d'entrypoint dans le conteneur
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# Copiez le script d'entrypoint dans le conteneur
COPY ldif/complete.ldif /ldap_user/

# Copiez le fichier docker-compose.yml dans le conteneur
COPY dind/docker-compose.yml /etc/docker/compose/docker-compose.yml

EXPOSE 389/tcp 636/tcp

# Définissez le script d'entrypoint comme point d'entrée
ENTRYPOINT ["/entrypoint.sh"]
