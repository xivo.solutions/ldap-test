#!/usr/bin/env python3

import random
from tqdm import tqdm
from faker import Faker

fake = Faker()

# Liste pour la création de valeurs uniques
generated_telephonenumber = []
generated_homePhones = []
generated_mobile = []
generated_last_name = []

CREATE_LDAP_USERS: bool = True
CREATE_XIVO_USERS: bool = True

LDAP_FILENAME: str = "ldap_data.ldif"
XIVO_FILENAME: str = "xivo_data.csv"

# Génère une liste de X utilisateurs uniques (Le max correspond à la plage de numéro interne).
TOTAL_ENTRIES: int = 30
INTERNAL_NUMBER_INTERVAL_START: int = 1600 #exclus

class FakeUserData:
    def __init__(self, first_name:str, last_name:str, mail:str, index:int):
        self.first_name = first_name
        self.last_name = last_name
        self.mail = mail
        self.index = index

class Department:
    def __init__(self, name: str, description: str, entry: str, jobs: list[str]):
        self.name = name
        self.description = description
        self.entry = entry
        self.jobs = jobs

class XivoCsvEntry:
    def __init__(self,
                entity_id: int,
                firstname: str,
                lastname: str,
                email: str,
                outgoing_caller_id: str,
                language: str,
                enabled: int,
                ring_seconds: int,
                simultaneous_calls: int,
                supervision_enabled: int,
                call_transfer_enabled: int,
                call_record_enabled: int,
                online_call_record_enabled: int,
                username: str,
                password: str,
                cti_profile_name: str,
                cti_profile_enabled: int,
                line_protocol: str,
                line_site: str,
                context: str,
                exten: str):
        self.entity_id = entity_id
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.outgoing_caller_id = outgoing_caller_id
        self.language = language
        self.enabled = enabled
        self.ring_seconds = ring_seconds
        self.simultaneous_calls = simultaneous_calls
        self.supervision_enabled = supervision_enabled
        self.call_transfer_enabled = call_transfer_enabled
        self.call_record_enabled = call_record_enabled
        self.online_call_record_enabled = online_call_record_enabled
        self.username = username
        self.password = password
        self.cti_profile_name = cti_profile_name
        self.cti_profile_enabled = cti_profile_enabled
        self.line_protocol = line_protocol
        self.line_site = line_site
        self.context = context
        self.exten = exten

    @staticmethod
    def build_header() -> str:
        return "entity_id," + \
            "firstname," + \
            "lastname,"  + \
            "email,"  + \
            "outgoing_caller_id," + \
            "language," + \
            "enabled,"  + \
            "ring_seconds," + \
            "simultaneous_calls," + \
            "supervision_enabled," + \
            "call_transfer_enabled," + \
            "call_record_enabled," + \
            "online_call_record_enabled," + \
            "username," + \
            "password," + \
            "cti_profile_name," + \
            "cti_profile_enabled," + \
            "line_protocol," + \
            "line_site," + \
            "context," + \
            "exten"

    def build_entry(self) -> str:
        return f"{self.entity_id}," + \
            f"{self.firstname}," + \
            f"{self.lastname}," + \
            f"{self.email}," + \
            f"{self.outgoing_caller_id}," + \
            f"{self.language}," + \
            f"{self.enabled}," + \
            f"{self.ring_seconds}," + \
            f"{self.simultaneous_calls}," + \
            f"{self.supervision_enabled}," + \
            f"{self.call_transfer_enabled}," + \
            f"{self.call_record_enabled}," + \
            f"{self.online_call_record_enabled}," + \
            f"{self.username}," + \
            f"{self.password}," + \
            f"{self.cti_profile_name}," + \
            f"{self.cti_profile_enabled}," + \
            f"{self.line_protocol}," + \
            f"{self.line_site}," + \
            f"{self.context}," + \
            f"{self.exten}"

def generate_fake_user_data(index: int) -> FakeUserData:
    first_name: str = fake.first_name()
    if index == 1001:
        Faker.seed(0)
    while True:
        last_name : str = fake.last_name()
        #last_name += chr((index // 1000) % 26 + 97) #le a derriere le nom de famille
        if last_name not in generated_last_name:
            generated_last_name.append(last_name)
            break
    last_name : str = generated_last_name[-1]
    mail :str = f"{first_name.lower()}.{last_name.lower()}@xivo-dev.com"
    return FakeUserData(
        first_name,
        last_name,
        mail,
        index
    )

def generate_ldif_entry(fake_user_data: FakeUserData, departments: list[Department]) -> str:
    full_name: str = fake_user_data.first_name + " " + fake_user_data.last_name
    uid: str = f"{fake_user_data.first_name[0].lower()}{fake_user_data.last_name.lower()}"

    # Génération d'un numéro mobile commençant par "06" ou "07" suivi de 8 chiffres
    while True:
        mobile = fake.random_element(elements=('06', '07')) + str(fake.random_number(8))
        if mobile not in generated_mobile:
            if len(mobile) == 10:
                generated_mobile.append(mobile)
                break
    # Génération d'un numéro résidentiel unique commençant par "01", "02", "03", "04", "05" ou "09" suivi de 8 chiffres
    while True:
        telephone_number = fake.random_element(elements=('01', '02', '03', '04', '05', '09')) + str(fake.random_number(8))
        if telephone_number not in generated_telephonenumber:
            if len(telephone_number) == 10:
                generated_telephonenumber.append(telephone_number)
                break

    # Génération d'un numéro interne unique entre 1000 et 5000
    while True:
        home_phone = fake.random_int(1000, 5000)
        if home_phone not in generated_homePhones:
            generated_homePhones.append(home_phone)
            break

    agent_group: int = 1

    password = fake.password(length=8, special_chars=False)

    chosen_dpt: Department = random.choice(departments)
    chosen_job: str = random.choice(chosen_dpt.jobs)

    ldif_entry: str = f"""
# {full_name}
dn: cn={full_name},ou={chosen_dpt.name},{{{{ LDAP_BASE_DN }}}}
objectclass: inetOrgPerson
cn: {full_name}
givenname: {fake_user_data.first_name}
sn: {fake_user_data.last_name}
uid: {uid}
userPassword: {password}
title: {chosen_job}
ou: {chosen_dpt.description}
mail: {fake_user_data.mail}
telephonenumber: {telephone_number}
mobile: {mobile}
homePhone: {home_phone}
pager: {agent_group}"""

    return ldif_entry

def generate_csv_entry(fake_user_data: FakeUserData) -> str:
    xivo_csv_entry : XivoCsvEntry = XivoCsvEntry(
        entity_id=1,
        firstname=fake_user_data.first_name,
        lastname=fake_user_data.last_name,
        email=fake_user_data.mail,
        outgoing_caller_id="default",
        language="fr_FR",
        enabled=1,
        ring_seconds=20,
        simultaneous_calls=2,
        supervision_enabled=1,
        call_transfer_enabled=0,
        call_record_enabled=0,
        online_call_record_enabled=0,
        username=fake_user_data.first_name[0].lower() + fake_user_data.last_name.lower(),
        password="1234",
        cti_profile_name="Supervisor",
        cti_profile_enabled=1,
        line_protocol="webrtc",
        line_site="default",
        context="default",
        exten=f"{INTERNAL_NUMBER_INTERVAL_START + fake_user_data.index}"
    )
    return xivo_csv_entry.build_entry()

def create_department(name, desc) -> Department:
    entry = f"""# Service {desc}
dn: ou={name},{{{{ LDAP_BASE_DN }}}}
objectclass: OrganizationalUnit
ou: {name}
description: {desc}
"""
    jobs = []
    for _ in range(5):
        jobs.append(fake.job())
    return Department(
        name,
        desc,
        entry,
        jobs
    )

def generate_department_entries() -> list[Department]:
    sales = create_department("sales", "Commerce")
    support = create_department("support", "Support")
    rd = create_department("rd", "Recherche & Développement")
    mkt = create_department("marketing", "Marketing")
    rh = create_department("rh", "Ressources Humaines")
    si = create_department("si", "Système d'Information")
    return [sales, support, rd, mkt, rh, si]

if __name__ == "__main__":
    fake_instance = Faker()
    ldif_entries = []
    csv_entries = []
    departments = generate_department_entries()

    if CREATE_LDAP_USERS:
        for dpt in departments:
            ldif_entries.append(dpt.entry)

    if CREATE_XIVO_USERS:
        csv_entries.append(XivoCsvEntry.build_header())

    with tqdm(total=TOTAL_ENTRIES, desc="Generating entries") as pbar:
        for i in range(1, TOTAL_ENTRIES + 1):
            fake_user_data = generate_fake_user_data(i)
            if CREATE_LDAP_USERS:
                ldif_entries.append(generate_ldif_entry(fake_user_data, departments))
            if CREATE_XIVO_USERS:
                csv_entries.append(generate_csv_entry(fake_user_data))
            pbar.update(1)

    if CREATE_XIVO_USERS:
        with open(XIVO_FILENAME, "w", encoding="utf-8") as file:
            file.write("\n".join(csv_entries))
        print(f"XiVO entries generated and saved to {XIVO_FILENAME}")

    if CREATE_LDAP_USERS:
        with open(LDAP_FILENAME, "w", encoding="utf-8") as file:
            file.write("\n".join(ldif_entries))
        print(f"LDAP entries generated and saved to {LDAP_FILENAME}")
