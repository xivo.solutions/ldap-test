# LDAP Test

Project which helps you to launch a LDAP server populated with a few users.
Currently it populates 3 users in the *OU* `sales` (the base DN is taken from the `LDAP_DOMAIN` env variable).
So if you use the commands below 
- the LDAP_DOMAIN is `test.avencall.com`
- and the base DN will be `dc=test,dc=avencall,dc=com`

There are 3 users in the OU sales (`ou=sales,dc=test,dc=avencall,dc=com`):

uid | password
 ---- | ---- 
mdesventes | superpass
rbonvendeur | superpass
cproforma | superpass

## Launch the LDAP server (DOCKER RUN)

Run the following command:
``` bash
docker run \
    --name ldap_validation \
    --rm \
    --privileged \
    -p 389:389 \
    -p 636:636 \
    -v /etc/hosts:/etc/hosts_custom \
    xivoxc/ldap-test:latest
```

Option :
``` bash
    -v /path_file/complete_export_client.ldif:/ldap_user/complete.ldif # Pour la modification des utilisateurs ldap
```

## Or docker-compose file (DOCKER-COMPOSE)

Go to root of this repository:

```
cd <path to>/ldap-test
```

Then launch the image with this command:
``` bash
docker-compose up -d 
```

Option :
``` yml
    - /path_file/complete_export_client.ldif:/ldap_user/complete.ldif # Pour la modification des utilisateurs ldap
```

### Checks
Check that it works (install the `ldap-utils` package on your laptop):

## LDAP port 389 :
```
ldapsearch -x -H ldap://ldap.test.avencall.com -D "cn=admin,dc=test,dc=avencall,dc=com" -w superpass -b "ou=sales,dc=test,dc=avencall,dc=com" "(name=Mich*)"
```

## LDAPS port 636 :
```
ldapsearch -x -H ldaps://ldap.test.avencall.com -D "cn=admin,dc=test,dc=avencall,dc=com" -w superpass -b "ou=sales,dc=test,dc=avencall,dc=com" "(name=Mich*)"
```

It should return you one result.