#!/bin/bash

# Fonction pour vérifier si le démon Docker est actif
check_docker_status() {
  if docker info &> /dev/null; then
    service docker restart 
    return 0  # Le démon Docker est actif
  else
    return 1  # Le démon Docker n'est pas actif
  fi
}

# Récupérer le certificat SSL et la clé privée depuis un site web
wget --no-check-certificate -O /certificates/ldap_test_avencall_com.crt https://dl.avencall.com/certificats_internes/_.test.avencall.com.fullchain > /dev/null 2>&1
wget --no-check-certificate -O /certificates/ldap_test_avencall_com.key https://dl.avencall.com/certificats_internes/_.test.avencall.com.key > /dev/null 2>&1
if [ -s /certificates/ldap_test_avencall_com.crt ] && [ -s /certificates/ldap_test_avencall_com.key ] && [ -s /certificates/dhkey.pem ]; then
  echo "Certificat et clé privée récupérés avec succès."
else 
  echo "Échec de la récupération du certificat ou de la clé privée."
  echo "Veuillez à avoir un accès à dl.avencall.com"
  exit 1
fi

if ! docker info &>/dev/null; then
  echo "Le service Docker n'est pas en cours d'exécution. Démarrage du service Docker..."
  service docker start 
fi

echo "127.0.1.1 ldap.test.avencall.com" >> /etc/hosts

# Vérifiez si l'adresse IP du conteneur est déjà présente dans le fichier /etc/hosts de la machine hôte
if ! sudo grep -q "ldap.test.avencall.com" /etc/hosts_custom; then
  # L'adresse IP n'est pas présente, ajoutez-la au fichier /etc/hosts de la machine hôte
  echo -e "\n#Ajout des redirections FQDN LDAP" >> /etc/hosts_custom
  echo "127.0.0.1 ldap.test.avencall.com" | sudo tee -a /etc/hosts_custom
else
  # L'adresse IP est déjà présente, ne faites rien
  echo "L'adresse IP du conteneur est déjà présente dans /etc/hosts de l'hôte"
fi

# Attendre que le service soit prêt
while ! check_docker_status; do
  sleep 1
done

# Lancement des containeurs
docker-compose -f /etc/docker/compose/docker-compose.yml up